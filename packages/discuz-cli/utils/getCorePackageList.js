const PACKAGE_CLI = '@discuzq/cli';
const PACKAGE_DESIGN = '@discuzq/design';
const PACKAGE_THEME = '@discuzq/theme';
const PACKAGE_SDK = '@discuzq/sdk';
const PACKAGE_VDITOR = '@discuzq/vditor';
const PACKAGE_IMPORT = '@discuzq/discuz-babel-plugin-import';
const PACKAGE_PLUGIN_LOADER = '@discuzq/discuz-plugin-loader';
const PACKAGE_PLUGIN_CENTER = '@discuzq/plugin-center';
const corePackages = [PACKAGE_CLI, PACKAGE_THEME, PACKAGE_DESIGN, PACKAGE_SDK, PACKAGE_VDITOR, PACKAGE_IMPORT, PACKAGE_PLUGIN_LOADER, PACKAGE_PLUGIN_CENTER];

module.exports = corePackages;