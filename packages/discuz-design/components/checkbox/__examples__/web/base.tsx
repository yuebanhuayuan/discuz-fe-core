import React from 'react';
import { Checkbox } from '@discuzq/design';

export default function CheckboxExample() {
  return (
    <div className="page">
      <div>
        <Checkbox defaultChecked>Checkbox</Checkbox>
      </div>
    </div>
  );
}

