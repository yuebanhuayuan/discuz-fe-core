import React from 'react';
import { Switch } from '@discuzq/design';

export default function SwitchExample() {
  return (
    <>
      <Switch defaultChecked color="#C4D4FF" />
      <Switch defaultChecked color="#3C85FF" />
      <Switch defaultChecked color="#C12A5D" />
    </>
  );
}
