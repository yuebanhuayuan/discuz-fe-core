import React from 'react';
import { Switch } from '@discuzq/design';

export default function SwitchExample() {
  return (
    <>
      <Switch defaultChecked size={18} />
      <Switch defaultChecked size={36} />
      <Switch defaultChecked size={54} />
    </>
  );
}
